import 'package:flutter/material.dart';
import 'package:graduapp/src/pages/ciclos_admin_page.dart';
import 'package:graduapp/src/pages/comprobante_page.dart';
import 'package:graduapp/src/pages/home_page.dart';
import 'package:graduapp/src/pages/login_page.dart';
import 'package:graduapp/src/pages/registro_page.dart';
import 'package:graduapp/src/pages/home_admin_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => LoginPage(),
    'comprobante': (BuildContext context) => ComprobantePage(),
    'registro': (BuildContext context) => RegistroPage(),
    'home': (BuildContext context) => HomePage(),
    'homeAdmin': (BuildContext context) => HomeAdminPage(),
    'ciclosAdmin': (BuildContext context) => CiclosAdminPage(),
  };
}
