import 'dart:convert';
import 'package:graduapp/src/models/ciclos_model.dart';
import 'package:graduapp/src/models/estudiantes_model.dart';
import 'package:graduapp/src/models/admins_model.dart';
import 'package:graduapp/src/models/solicitudes_model.dart';
import 'package:graduapp/src/share_prefs/preferencias_usuario.dart';
import 'package:http/http.dart' as http;

class LoginProvider {
  final String _url = "https://gradu-app.firebaseio.com/";

  Future<bool> cargarCuenta(String nCtrl, String pass) async {
    String url = '$_url/estudiantes/$nCtrl.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return false;

    EstudiantesModel estudiante = EstudiantesModel.fromJson(decodedData);

    PreferenciasUsuario _prefs = new PreferenciasUsuario();
    _prefs.name = estudiante.nombre;
    _prefs.apellidoM = estudiante.apM;
    _prefs.apellidoP = estudiante.apP;
    if (estudiante.solicitud != "null") {
      _prefs.solicitud = estudiante.solicitud;

      String urlSol = "$_url/solicitudes/${estudiante.solicitud}.json";

      final respSol = await http.get(urlSol);
      final Map<String, dynamic> decData = json.decode(respSol.body);

      SolicitudesModel solicitud = SolicitudesModel.fromJson(decData);
      _prefs.estado = solicitud.estado;
      _prefs.urlImagen = solicitud.foto;

      if (solicitud.estado == 2) {
        _prefs.razon = solicitud.notas;
      }

      if (solicitud.estado == 1) {
        _prefs.urlPdf = solicitud.urlPdf;
      }
    } else {
      _prefs.estado = 4;
    }

    String urlCiclo = "$_url/ciclos.json";

    final respCic = await http.get(urlCiclo);
    final Map<String, dynamic> decCic = json.decode(respCic.body);

    decCic.forEach((key, value) {
      CiclosModel ciclosModel = new CiclosModel.fromJson(value);
      if (ciclosModel.estado == true) {
        _prefs.ciclo = key;
      }
    });

    if (estudiante.contrasea == pass) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> cargarAdmin(String usr, String pass) async {
    String url = _url + "admins.json";
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return false;

    //AdminsModel adminsModel;
    bool acceso = false;
    decodedData.forEach((key, value) {
      AdminsModel adminsModel = AdminsModel.fromJson(value);
      if (adminsModel.usuario == usr && adminsModel.password == pass) {
        acceso = true;
      }
    });

    return acceso;
  }
}
