import 'package:http/http.dart' as http;
import 'package:graduapp/src/models/ciclos_model.dart';
import 'dart:convert';

class CicloProvider {
  final String _url = "https://gradu-app.firebaseio.com";

  Future<List<String>> cargarCiclos() async {
    String url = '$_url/ciclos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return null;

    List<String> _ciclos = new List();
    decodedData.forEach((key, value) {
      CiclosModel ciclosModel = new CiclosModel.fromJson(value);
      ciclosModel.id = key;
      _ciclos.add(ciclosModel.ciclo);
    });

    _ciclos.sort((a, b) => a.compareTo(b));

    return _ciclos;
  }

  Future<List<CiclosModel>> getCiclos() async {
    String url = '$_url/ciclos.json';
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return null;

    List<CiclosModel> _ciclos = new List();
    decodedData.forEach((key, value) {
      CiclosModel ciclosModel = new CiclosModel.fromJson(value);
      ciclosModel.id = key;
      _ciclos.add(ciclosModel);
    });

    return _ciclos;
  }

  Future<bool> setCiclo(CiclosModel ciclo) async {
    String url = '$_url/ciclos/${ciclo.id}.json';

    ciclo.estado = false;
    /*
      Aquí en la parte de abajo es donde se sobreescribe el modelo que tienes en la BD con el que se modificó en la App
    */
    final resp = await http.put(url, body: ciclosModelToJson(ciclo));
    if (resp == null) return null;

    return true;
  }

  Future<bool> nuevoCiclo(String ciclo) async {
    String url = '$_url/ciclos.json';
    CiclosModel ciclosModel = new CiclosModel();
    ciclosModel.ciclo = ciclo;
    ciclosModel.estado = true;

    final resp = await http.post(url, body: ciclosModelToJson(ciclosModel));
    if (resp == null) return null;

    return true;
  }
}
