import 'package:graduapp/src/models/estudiantes_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:graduapp/src/share_prefs/preferencias_usuario.dart';

class CuentaProvider {

  final String _url = 'https://gradu-app.firebaseio.com/estudiantes';
  final _prefs = new PreferenciasUsuario();
  Future<EstudiantesModel> crearCuenta (String nombre,String apP,String apM,String nC,String carrera,String contrasea) async {

    String url = '$_url/$nC.json';
    final res = await http.get(url);
    String json = '';

    if(res.body != 'null'){
      return null;
    }else{

       json = '{"nombre":"$nombre", "ap_p":"$apP", "ap_m":"$apM", "carrera":"$carrera", "contraseña":"$contrasea", "solicitud":"null"}';

      EstudiantesModel cuenta = new EstudiantesModel(
        nombre: nombre,
        apM: apM,
        apP: apP,
        carrera: carrera,
        contrasea: contrasea,
      );

      url = '$_url/$nC.json';

      final resp = await http.put(url, body: json);   
      //_prefs.name = nombre;

      return cuenta;      
       }
    

  }


}