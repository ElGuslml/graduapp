import 'dart:convert';
import 'package:graduapp/src/models/estudiantes_model.dart';
import 'package:graduapp/src/models/solicitudes_model.dart';
import 'package:http/http.dart' as http;
import 'package:graduapp/src/models/card_solicitud_model.dart';

class SolicitudesProvider {
  final String _url = "https://gradu-app.firebaseio.com/";

  Future<List<CardSolicitudModel>> getSolicitudes(bool _sinRevisar,
      bool _aceptadas, bool _rechazadas, String _cicloActual) async {
    String url = _url + "solicitudes.json";
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return null;

    String url2 = _url + "estudiantes.json";
    final resp2 = await http.get(url2);
    final Map<String, dynamic> decodedData2 = json.decode(resp2.body);

    if (decodedData2 == null) return null;

    String url3 = _url + "ciclos.json";
    final resp3 = await http.get(url3);
    final Map<String, dynamic> decodedData3 = json.decode(resp3.body);

    if (decodedData3 == null) return null;

    List<CardSolicitudModel> lista = new List();

    String idCiclo = "";

    decodedData3.forEach((key, value) {
      if (value["ciclo"] == _cicloActual) {
        idCiclo = key;
      }
    });

    decodedData.forEach((key, value) {
      SolicitudesModel solicitudesModel = new SolicitudesModel.fromJson(value);
      CardSolicitudModel cardSolicitudModel = new CardSolicitudModel();
      cardSolicitudModel.id = key;
      cardSolicitudModel.estado = solicitudesModel.estado;
      cardSolicitudModel.numC = solicitudesModel.nControl;
      cardSolicitudModel.urlImg = solicitudesModel.foto;

      decodedData2.forEach((key, value) {
        if (solicitudesModel.nControl == key) {
          EstudiantesModel estudiantesModel =
              new EstudiantesModel.fromJson(value);
          cardSolicitudModel.nombreAl = estudiantesModel.nombre +
              " " +
              estudiantesModel.apP +
              " " +
              estudiantesModel.apM;
          cardSolicitudModel.carrera = estudiantesModel.carrera;

          if (idCiclo != "") {
            if (_sinRevisar &&
                solicitudesModel.estado == 0 &&
                solicitudesModel.ciclo == idCiclo) {
              lista.add(cardSolicitudModel);
            }

            if (_aceptadas &&
                solicitudesModel.estado == 1 &&
                solicitudesModel.ciclo == idCiclo) {
              lista.add(cardSolicitudModel);
            }

            if (_rechazadas &&
                solicitudesModel.estado == 2 &&
                solicitudesModel.ciclo == idCiclo) {
              lista.add(cardSolicitudModel);
            }
          } else {
            if (_sinRevisar && solicitudesModel.estado == 0) {
              lista.add(cardSolicitudModel);
            }

            if (_aceptadas && solicitudesModel.estado == 1) {
              lista.add(cardSolicitudModel);
            }

            if (_rechazadas && solicitudesModel.estado == 2) {
              lista.add(cardSolicitudModel);
            }
          }
        }
      });
    });

    return lista;
  }

  Future<bool> rechazarSol(CardSolicitudModel solicitud, String nota) async {
    //Copia desde aquí
    String url = _url + "estudiantes/${solicitud.numC}.json";
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return null;
    EstudiantesModel estudiantesModel =
        new EstudiantesModel.fromJson(decodedData);

    String idSol = "";
    idSol = estudiantesModel.solicitud;

    String url2 = _url + "solicitudes/$idSol.json";
    final resp2 = await http.get(url2);
    final Map<String, dynamic> decodedData2 = json.decode(resp2.body);

    if (decodedData2 == null) return null;
    SolicitudesModel solicitudesModel =
        new SolicitudesModel.fromJson(decodedData2);

    //Hasta aquí
    solicitudesModel.estado = 2;
    solicitudesModel.notas = nota;

    final respFinal =
        await http.put(url2, body: solicitudesModelToJson(solicitudesModel));

    print(respFinal);

    return true;
  }

  Future<bool> aceptarSol(CardSolicitudModel solicitud, String urlPdf) async {
    //Copia desde aquí
    String url = _url + "estudiantes/${solicitud.numC}.json";
    final resp = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return null;
    EstudiantesModel estudiantesModel =
        new EstudiantesModel.fromJson(decodedData);

    String idSol = "";
    idSol = estudiantesModel.solicitud;

    String url2 = _url + "solicitudes/$idSol.json";
    final resp2 = await http.get(url2);
    final Map<String, dynamic> decodedData2 = json.decode(resp2.body);

    if (decodedData2 == null) return null;
    SolicitudesModel solicitudesModel =
        new SolicitudesModel.fromJson(decodedData2);

    //Hasta aquí
    solicitudesModel.estado = 1;
    solicitudesModel.urlPdf = urlPdf;
    //solicitudesModel.notas = nota;

    final respFinal =
        await http.put(url2, body: solicitudesModelToJson(solicitudesModel));

    print(respFinal);

    return true;
  }

  /*
    Lo que vas a hacer primero, es subir la imagen a un servidor por medio de una API, la API te debe regresar un URL
    para que veas un ejemplo puedes ir a comprobante_provider.dart, ahí ya está comentado los pasos que hice para subir una imagen
    a un servidor con una API y me regresa una URL

    Después ya que tengas la URL vas a hacer casi lo que yo hice acá arriba en el metodo rechazarSol. Vas a recibir CardSolicitudModel solicitud solamente
    Vas a hacer una petición como yo, ahí te dejé de donde a donde puedes copiar el código.
    Después vas a hacer lo mismo que hice después pero vas a poner en vez de
      solicitudesModel.estado = 2, lo vas a cambiar a 1 porque estás aceptando la solicitud
    Y vas a poner la URL del PDF en
      solicitudesModel.pdfUrl
    
    Y ya después vas a hacer lo mismo que hice lo de final respFinal hasta el final (return true)

    Y listo we... No vayas a cagar la BD plox
  */
}
