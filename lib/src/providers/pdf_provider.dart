import 'dart:convert';
import 'dart:io';
import 'package:graduapp/src/models/estudiantes_model.dart';
import 'package:graduapp/src/models/solicitudes_model.dart';
import 'package:graduapp/src/share_prefs/preferencias_usuario.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';

class ComprobanteProvider {
  Future<bool> subirImagen(File imagen) async {
    /*
      Aquí es donde se manda a la API la imagen para que me regrese un link donde poder acceder a esa imagen.
    */
    final url = Uri.parse(
        'https://api.cloudinary.com/v1_1/dac1uot9b/image/upload?upload_preset=qa9xycz5');
    final mimeType = mime(imagen.path).split('/');
    //Aquí hago la preparación para la petición HTTP para subir la imagen al servidor
    final imageUploadRequest = http.MultipartRequest('POST', url);
    //Aquí no sé qué carajos sucede, es algo así como de que le dices de qué tipo es el archivo que vas a subir y el formato
    final file = await http.MultipartFile.fromPath('file', imagen.path,
        contentType: MediaType(mimeType[0], mimeType[1]));
    //Aquí se agrega el archivo a la petición HTTP
    imageUploadRequest.files.add(file);
    //Aquí ya por fin mandas el Archivo al servidor
    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);
    //Estos son errores para comprobar qué show
    if (resp.statusCode != 200 && resp.statusCode != 201) {
      print('Algo salió mal');
      print(resp.body);
      return null;
    }
    //Si todo salió bien entonces deberías llegar hasta acá y te regresará un url
    final respData = json.decode(resp.body);
    print(respData);
    //Aquí guardo el URL que me regresó la API del servidor donde subí la imagen
    String _urlImagen = respData['secure_url'];
    //Aquí solo guardo la URL de solicitudes
    final String _url = "https://gradu-app.firebaseio.com/solicitudes.json";
    //Esto lo hago para acceder a las preferencias y poder obtener todo lo que la APP ya tiene conocimiento como numero de control, ciclo escolar, etc.
    final _prefs = new PreferenciasUsuario();
    //Aquí creo un nuevo objeto de SolicitudesModel para poder crear mi solicitud
    SolicitudesModel solicitudesModel = new SolicitudesModel();
    //Aquí le doy sus propiedades al objeto de SolicitudesModel, este es el que se subirá a la BD
    solicitudesModel.ciclo = _prefs.ciclo;
    solicitudesModel.nControl = _prefs.numC;
    solicitudesModel.notas = "NULL";
    solicitudesModel.estado = 0;
    solicitudesModel.foto = _urlImagen;
    //Aquí ya solo mando la petición con el objeto solicitudesModel convertido a JSON, OJO!! ESTO NO LO HAGAS, esto es distinto, en vez de POST tú lo harás PUT
    //Pero para eso ocupas saber el ID de la solicitud
    final respuesta =
        await http.post(_url, body: solicitudesModelToJson(solicitudesModel));

    final decodedData = json.decode(respuesta.body);

    final idSolicitud = decodedData['name'];

    final String _urlEstudiante =
        "https://gradu-app.firebaseio.com/estudiantes/${_prefs.numC}.json";

    final respEstu = await http.get(_urlEstudiante);
    final Map<String, dynamic> decodedD = json.decode(respEstu.body);

    EstudiantesModel estudiantesModel = new EstudiantesModel.fromJson(decodedD);
    estudiantesModel.solicitud = idSolicitud;

    final respFinal = await http.put(_urlEstudiante,
        body: estudiantesModelToJson(estudiantesModel));

    print(respFinal);

    return true;
  }
}
