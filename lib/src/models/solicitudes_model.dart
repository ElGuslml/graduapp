import 'dart:convert';

SolicitudesModel solicitudesModelFromJson(String str) =>
    SolicitudesModel.fromJson(json.decode(str));

String solicitudesModelToJson(SolicitudesModel data) =>
    json.encode(data.toJson());

class SolicitudesModel {
  String ciclo;
  int estado;
  String foto;
  String nControl;
  String notas;
  String urlPdf;

  SolicitudesModel({
    this.ciclo,
    this.estado,
    this.foto,
    this.nControl,
    this.notas,
    this.urlPdf,
  });

  factory SolicitudesModel.fromJson(Map<String, dynamic> json) =>
      SolicitudesModel(
        ciclo: json["ciclo"],
        estado: json["estado"],
        foto: json["foto"],
        nControl: json["n_control"],
        notas: json["notas"],
        urlPdf: json["urlPdf"],
      );

  Map<String, dynamic> toJson() => {
        "ciclo": ciclo,
        "estado": estado,
        "foto": foto,
        "n_control": nControl,
        "notas": notas,
        "urlPdf": urlPdf,
      };
}
