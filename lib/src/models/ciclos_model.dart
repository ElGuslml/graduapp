import 'dart:convert';

CiclosModel ciclosModelFromJson(String str) =>
    CiclosModel.fromJson(json.decode(str));

String ciclosModelToJson(CiclosModel data) => json.encode(data.toJson());

class CiclosModel {
  String id;
  String ciclo;
  bool estado;

  CiclosModel({
    this.id,
    this.ciclo,
    this.estado,
  });

  factory CiclosModel.fromJson(Map<String, dynamic> json) => CiclosModel(
        id: json["id"],
        ciclo: json["ciclo"],
        estado: json["estado"],
      );

  Map<String, dynamic> toJson() => {
        "ciclo": ciclo,
        "estado": estado,
      };
}
