import 'dart:convert';

EstudiantesModel estudiantesModelFromJson(String str) => EstudiantesModel.fromJson(json.decode(str));

String estudiantesModelToJson(EstudiantesModel data) => json.encode(data.toJson());

class EstudiantesModel {
    String apM;
    String apP;
    String carrera;
    String contrasea;
    String nombre;
    String solicitud;

    EstudiantesModel({
        this.apM,
        this.apP,
        this.carrera,
        this.contrasea,
        this.nombre,
        this.solicitud,
    });

    factory EstudiantesModel.fromJson(Map<String, dynamic> json) => EstudiantesModel(
        apM: json["ap_m"],
        apP: json["ap_p"],
        carrera: json["carrera"],
        contrasea: json["contraseña"],
        nombre: json["nombre"],
        solicitud: json["solicitud"],
    );

    Map<String, dynamic> toJson() => {
        "ap_m": apM,
        "ap_p": apP,
        "carrera": carrera,
        "contraseña": contrasea,
        "nombre": nombre,
        "solicitud": solicitud,
    };
}
