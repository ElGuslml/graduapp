

class CardSolicitudModel {

  String nombreAl;
  String numC;
  String carrera;
  int estado;
  String id;
  String urlImg;

  CardSolicitudModel({
    this.nombreAl,
    this.numC,
    this.carrera,
    this.estado,
    this.id,
    this.urlImg
  });

}