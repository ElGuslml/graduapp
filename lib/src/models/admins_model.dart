import 'dart:convert';

AdminsModel adminsModelFromJson(String str) => AdminsModel.fromJson(json.decode(str));

String adminsModelToJson(AdminsModel data) => json.encode(data.toJson());

class AdminsModel {
    String password;
    String usuario;

    AdminsModel({
        this.password,
        this.usuario,
    });

    factory AdminsModel.fromJson(Map<String, dynamic> json) => AdminsModel(
        password: json["password"],
        usuario: json["usuario"],
    );

    Map<String, dynamic> toJson() => {
        "password": password,
        "usuario": usuario,
    };
}
