import 'package:flutter/material.dart';
import 'package:graduapp/src/providers/login_provider.dart';
import 'package:graduapp/src/share_prefs/preferencias_usuario.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  
  bool _boolAdmin = false;
  String _numC;
  String _pass;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: Padding(
       padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
     child: Center(
       child: SingleChildScrollView(
         child: Column(
           children: <Widget>[
            SizedBox(height: 130,),
              Image(image: AssetImage('assets/images/Itmorelia.png'),height: 150,),
            SizedBox(height: 20,),
              _crearNCtrl(),
            SizedBox(height: 5,),
              _crearPass(),  
            SizedBox(height: 10,),
              _crearBoton(),
            SizedBox(height: 10,),
            _crearBoton2(),
            SizedBox(height: 10,),
            /*InkWell(child: Text('Crear una cuenta ->'),
            onTap: (){
              Navigator.pushNamed(context, 'registro');
            },
          ),*/
              _crearRow(),
              ],
        ),
       ),
     ),
     )
    );
  }

  Widget _crearRow() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: <Widget>[
          Checkbox(
            value: _boolAdmin,
            onChanged: (value){
              _boolAdmin = value;
              setState(() {
                
              });
            },
          ),
          Text('Administrador'),
          
        ],
      ),
    );
  }

 Widget _crearNCtrl() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Número de control',
          ),
          onChanged: (text) {
            _numC = text;
          },
        ),
    );
      }
    );
  }


  Widget _crearPass() {
    
    return StreamBuilder(
    builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20,),
      child: TextField(
          obscureText: true,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Password',
            suffixIcon: Icon( Icons.visibility),
            //counterText: snapshot.data,
            errorText: snapshot.error,
          ),
          onChanged: (text) {
            _pass = text;
          },
        ),
    );
    }
    );
    
  }

  Widget _crearBoton() {
    
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
        child: SizedBox(
          width: double.infinity,
          child: FlatButton(  
              child: Text('Login'),
              color: Color.fromRGBO(255, 193, 7, 1),
              textColor: Color.fromRGBO(247, 245, 225, 1),
              onPressed: () async {
                
                LoginProvider loginProvider = new LoginProvider();
                
                if(_boolAdmin == true){
                  final resp = await loginProvider.cargarAdmin(_numC, _pass);
                  if(resp == true){
                    final _prefs = new PreferenciasUsuario();
                    _prefs.numC = _numC;
                    Navigator.pushNamed(context, 'homeAdmin');
                  }else{
                    mostrarAlerta();
                  }
                }else{
                  
                  final resp = await loginProvider.cargarCuenta(_numC, _pass);
                  if(resp == true) {
                    final _prefs = new PreferenciasUsuario();
                    _prefs.numC = _numC;
                    print(_prefs.numC);
                    Navigator.pushNamed(context, 'home');
                  }else{
                    mostrarAlerta();
                  }

                }


              }   
            ),
        ),
      );
  }

  Widget _crearBoton2() {
    
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
        child: SizedBox(
          width: double.infinity,
          child: FlatButton(  
              child: Text('Crear cuenta'),
              color: Color.fromRGBO(211, 47, 47, 1),
              textColor: Color.fromRGBO(247, 245, 225, 1),
              onPressed: () {
                Navigator.pushNamed(context, 'registro');
              }   
            ),
        ),
      );
  }

  

  Future mostrarAlerta() async {

    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
        title: Text('No se pudo iniciar sesión'),
        content: SingleChildScrollView(
          child: Text('Tu número de control o contraseña son incorrectos.'),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Aceptar'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      );
      } 
    );

  }

  }



