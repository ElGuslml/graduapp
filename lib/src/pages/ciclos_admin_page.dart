import 'package:flutter/material.dart';
import 'package:graduapp/src/models/ciclos_model.dart';
import 'package:graduapp/src/providers/ciclo_provider.dart';

class CiclosAdminPage extends StatefulWidget {
  CiclosAdminPage({Key key}) : super(key: key);

  @override
  _CiclosAdminPageState createState() => _CiclosAdminPageState();
}

class _CiclosAdminPageState extends State<CiclosAdminPage> {
  CicloProvider cicloProvider = new CicloProvider();
  bool _cicloActivo = false;
  String _cicloEscolar = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ciclos'),
      ),
      drawer: Drawer(
          child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Administrador',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  'admin',
                  style: TextStyle(color: Colors.grey, fontSize: 14),
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1.0,
          ),
          ListTile(
            leading: Icon(Icons.loop),
            title: Text('Ciclos escolares'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.chrome_reader_mode),
            title: Text('Solicitudes'),
            onTap: () {
              Navigator.pushNamed(context, 'homeAdmin');
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Cerrar Sesión'),
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
          )
        ],
      )),
      body: _builderF(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          print(_cicloActivo);
          if (_cicloActivo == true) {
            mostrarAlerta();
          } else {
            mostrarAlerta3();
          }
        },
      ),
    );
  }

  Widget _builderF() {
    return new FutureBuilder(
      future: cicloProvider.getCiclos(),
      builder:
          (BuildContext context, AsyncSnapshot<List<CiclosModel>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return CircularProgressIndicator();
            break;
          case ConnectionState.done:
            final ciclos = snapshot.data;
            if (ciclos.length < 1) {
              return Text("No hay ciclos");
            } else {
              return ListView.builder(
                shrinkWrap: true,
                itemCount: ciclos.length,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, i) {
                  return _ciclos(ciclos[i]);
                },
              );
            }
            break;
          default:
        }
      },
    );
  }

  Widget _ciclos(CiclosModel ciclo) {
    if (ciclo.estado == true) {
      _cicloActivo = true;
    }

    return Container(
      padding: EdgeInsets.all(20),
      child: SizedBox(
        width: double.infinity,
        child: Card(
          elevation: 4,
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Ciclo Escolar: ${ciclo.ciclo}",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 22.0),
                ),
                textCic(ciclo.estado),
                buttAct(ciclo),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget textCic(bool estado) {
    if (estado) {
      return Text(
        "Estado: Activo",
        style: TextStyle(fontSize: 18.0),
      );
    } else {
      return Text(
        "Estado: Inactivo",
        style: TextStyle(fontSize: 18.0),
      );
    }
  }

  Widget buttAct(CiclosModel ciclo) {
    if (ciclo.estado) {
      return FlatButton(
        child: Text(
          'Terminar',
          style: TextStyle(color: Colors.green),
          textAlign: TextAlign.left,
        ),
        onPressed: () {
          mostrarAlerta2(ciclo);
        },
      );
    } else {
      return SizedBox.shrink();
    }
  }

  mostrarAlerta() {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                title: Text('Aviso'),
                content: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Text(
                          'Primero debes terminar el ciclo escolar activo para agregar uno nuevo')
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Aceptar'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            },
          );
        });
  }

  mostrarAlerta2(CiclosModel ciclo) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                title: Text('Aviso'),
                content: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Text(
                          '¿Seguro de que desea dar por terminado este ciclo escolar? \n\nNo podrá darlo de alta nuevamente.')
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancelar'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('Aceptar'),
                    onPressed: () async {
                      final resp = await cicloProvider.setCiclo(ciclo);
                      if (resp != null) {
                        _cicloActivo = false;
                        handleState();
                      }
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            },
          );
        });
  }

  mostrarAlerta3() {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                title: Text('Nuevo ciclo escolar'),
                content: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      TextField(
                        textCapitalization: TextCapitalization.sentences,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                          labelText: 'Ciclo escolar',
                        ),
                        onChanged: (text) {
                          _cicloEscolar = text;
                        },
                      )
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancelar'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('Agregar'),
                    onPressed: () async {
                      cicloProvider.nuevoCiclo(_cicloEscolar);
                      handleState();
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            },
          );
        });
  }

  void handleState() {
    setState(() {});
  }
}
