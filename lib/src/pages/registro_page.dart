import 'package:flutter/material.dart';
import 'package:graduapp/src/providers/cuenta_provider.dart';

class RegistroPage extends StatefulWidget {
  RegistroPage({Key key}) : super(key: key);

  @override
  _RegistroPageState createState() => _RegistroPageState();
  
}

class _RegistroPageState extends State<RegistroPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  CuentaProvider cuenta = new CuentaProvider();
  List _carreras = ["Sistemas Computacionales", "Gestión Empresarial", "Industrial", "Administración"];
  
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCarrer;

  @override
  void initState() { 
    
    _dropDownMenuItems = getDropDownMenuItems();
    _currentCarrer = _dropDownMenuItems[0].value;
    super.initState();
    
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems(){
    List<DropdownMenuItem<String>> items = new List();
    for (String carrera in _carreras){
      items.add(new DropdownMenuItem(
        value: carrera,
        child: new Text(carrera),
      ));
    }
    return items;
  }

  String nombre="";
  String apP="";
  String apM="";
  String nC="";
  String carrera="";
  String email="";
  String pass="";


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      
      child: Scaffold(
        body: Padding(
         padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: SingleChildScrollView(
            child: Center(
            child: Form(
              key: _formKey,
              autovalidate: _autoValidate,
            child: Column(
              children: <Widget>[
                SizedBox(height: 30,),
                Text('Crear una nueva cuenta'),
                SizedBox(height: 10,),
                  _crearNombre(),
                  _crearAPP(),
                  _crearAPM(),
                  _crearNC(),
                  SizedBox(height: 5,),
                  _crearCarrera(),
                  SizedBox(height: 10,),
                  //_crearEmail(),
                  _crearPass(),
                  SizedBox(height: 10),
                  _crearPass1(),
                  SizedBox(height: 10,),
                  _crearBoton(),
                  SizedBox(height: 10,),
                  InkWell(child: Text('¿Ya tienes una cuenta? Iniciar sesión', style: TextStyle(color: Colors.blue),),
                    onTap: (){
                      Navigator.pushNamed(context, '/');
                    },
                    )
                  
                  
                  



              ],
            )
            )
            )
            )
        )
      ),
    );
  }

 Widget _crearNombre() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Nombre(s)',
            hintText: 'Carlos Alejandro',
          ),
          onChanged: (texto){
            nombre = texto;
          }
        ),
    );
      }
    );
  }

Widget _crearAPP() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Apellido Paterno',
            hintText: 'Galván',
          ),
          onChanged: (texto){
            apP = texto;
          },
        ),
    );
      }
    );
  }

  Widget _crearAPM() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Apellido Materno',
            hintText: 'Vega',
          ),
          onChanged: (texto){
            apM = texto;
          },
        ),
    );
      }
    );
  }

  Widget _crearNC() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Número de control',
            hintText: '16120509',
          ),
          onChanged: (texto){
            nC = texto;
          },
        ),
    );
      }
    );
  }

  Widget _crearCarrera() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
   /* 
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      //color: Colors.grey,
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Carrera',
            hintText: 'Sistemas Computacionales',
          ),
          onChanged: (texto){
            carrera = texto;
          },
        ),
    );
     */
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 65, vertical: 5),
      decoration: boxDecoration(),
      child: DropdownButton(
        value: _currentCarrer,
        items: _dropDownMenuItems, 
        onChanged: changedDropDownItem,
        
      ),
    );

      }
    );
  }

  BoxDecoration boxDecoration(){
    return BoxDecoration(
      border: Border.all(
        width: 0.5
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(5.0),
      )
    );
  }

  void changedDropDownItem(String carreraSelected){
    setState(() {
      _currentCarrer = carreraSelected;
    });
  }

  Widget _crearEmail() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Correo Electrónico',
            hintText: 'carlosgalvn97@gmail.com',
            suffixIcon: Icon(Icons.arrow_drop_down),
          ),
          onChanged: (texto){
            email = texto;
          },
        ),
    );
      }
    );
  }

   Widget _crearPass() {
    
    return StreamBuilder(
    builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20,),
      child: TextField(
          obscureText: true,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Contraseña',
            hintText: '*******',
            suffixIcon: Icon( Icons.visibility),
            //counterText: snapshot.data,
            errorText: snapshot.error,
          ),
          onChanged: (texto){
            pass = texto;
          } ,
        ),
    );
    }
    );
    
  }

  Widget _crearPass1() {
    
    return StreamBuilder(
    builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20,),
      child: TextField(
          obscureText: true,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Contraseña',
            hintText: '*******',
            suffixIcon: Icon( Icons.visibility),
            //counterText: snapshot.data,
            errorText: snapshot.error,
          ),
        ),
    );
    }
    );
    
  }

  Widget _crearBoton() {
    
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
        child: SizedBox(
          width: double.infinity,
          child: FlatButton(  
              child: Text('Registrar'),
              color: Color.fromRGBO(255, 193, 7, 1),
              textColor: Color.fromRGBO(247, 245, 225, 1),
              onPressed: () async {
                var result = await cuenta.crearCuenta(nombre,apP,apM,nC,_currentCarrer,pass);
                if(result == null){
                  showDialog(
                    context: context,
                    builder: (context){
                      return _alertaDialogo();
                    }
                    );
                }else{
                    Navigator.pushNamed(context, '/');
                }
              }   
            ),
        ),
      );
  }

  Widget _alertaDialogo() {

    return AlertDialog(
      title: Text('¡Aviso!'),
      content: Text('Este número de control ya se encuentra registrado.'),
      actions: <Widget>[
        FlatButton(
          child: Text('Aceptar'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }

}
