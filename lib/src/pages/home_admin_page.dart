//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:graduapp/src/providers/ciclo_provider.dart';
import 'package:graduapp/src/providers/solicitudes_provider.dart';
import 'package:graduapp/src/models/card_solicitud_model.dart';
import 'dart:io';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';
import 'package:graduapp/src/share_prefs/preferencias_usuario.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:path_provider/path_provider.dart';

class HomeAdminPage extends StatefulWidget {
  HomeAdminPage({Key key}) : super(key: key);

  @override
  _HomeAdminPageState createState() => _HomeAdminPageState();
}

class _HomeAdminPageState extends State<HomeAdminPage> {
  Icon cusIcon = Icon(Icons.search);
  Widget cusSearchBar = Text("Solicitudes");
  String _textSearch = "";
  bool _boolOk = false;
  List _ciclo = ["2019-1", "2019-2"];
  String _cicloActual = "";
  bool _sinRevisar = true;
  bool _aceptadas = false;
  bool _rechazadas = false;
  FirebaseStorage storage;
  String _nota = "";
  String urlPdf = "";

  PreferenciasUsuario prefs = new PreferenciasUsuario();
  SolicitudesProvider solicitudesProvider = new SolicitudesProvider();
  List<DropdownMenuItem<String>> _dropDownMenuItems;

  @override
  void initState() {
    _asyncMetodo();
    _dropDownMenuItems = getDropDownMenuItems();
    //_cicloActual = _dropDownMenuItems[0].value;
    super.initState();
  }

  _asyncMetodo() async {
    CicloProvider cicloProvider = new CicloProvider();
    _ciclo = await cicloProvider.cargarCiclos();
    _dropDownMenuItems = getDropDownMenuItems();
    _cicloActual = _dropDownMenuItems[_dropDownMenuItems.length - 1].value;
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String ciclo in _ciclo) {
      items.add(new DropdownMenuItem(
        value: ciclo,
        child: Text(ciclo),
      ));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(250, 250, 250, 1),
      appBar: AppBar(
        title: cusSearchBar,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {
              mostrarAlerta();
            },
          )
        ],
      ),
      drawer: Drawer(
          child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Administrador',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                  'admin',
                  style: TextStyle(color: Colors.grey, fontSize: 14),
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1.0,
          ),
          ListTile(
            leading: Icon(Icons.loop),
            title: Text('Ciclos escolares'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.chrome_reader_mode),
            title: Text('Solicitudes'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Cerrar Sesión'),
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
          )
        ],
      )),
      body: _builderF(),
    );
  }

  Widget _builderF() {
    return FutureBuilder(
      future: solicitudesProvider.getSolicitudes(
          _sinRevisar, _aceptadas, _rechazadas, _cicloActual),
      builder: (BuildContext context,
          AsyncSnapshot<List<CardSolicitudModel>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return CircularProgressIndicator();
            break;
          case ConnectionState.done:
            final solicitudes = snapshot.data;
            if (solicitudes.length < 1) {
              return Text('No hay solicitudes');
            } else {
              return ListView.builder(
                shrinkWrap: true,
                itemCount: solicitudes.length,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, i) {
                  return _solicitud(solicitudes[i]);
                },
              );
            }
            break;
          default:
        }
      },
    );
  }

  Widget _solicitud(CardSolicitudModel solicitud) {
    return Container(
      padding: EdgeInsets.all(20),
      child: SizedBox(
        width: double.infinity,
        child: Card(
          elevation: 4,
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  solicitud.nombreAl,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 18.0),
                ),
                Text(
                  'Numero de control: ${solicitud.numC}',
                  textAlign: TextAlign.left,
                ),
                Text(
                  'Carrera: ${solicitud.carrera}',
                  textAlign: TextAlign.left,
                ),
                //Text('Estado: ${solicitud.estado}'),
                textCarrera(solicitud.estado),
                buttSol(solicitud)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buttSol(CardSolicitudModel solicitud) {
    if (solicitud.estado == 0) {
      return FlatButton(
        child: Text(
          'Ver',
          style: TextStyle(color: Colors.green),
          textAlign: TextAlign.left,
        ),
        onPressed: () {
          mostrarAlerta2(solicitud.urlImg, solicitud.nombreAl, solicitud.numC,
              solicitud.carrera, solicitud);
          //solicitudesProvider.getSolicitudes(1);
        },
      );
    } else {
      return SizedBox.shrink();
    }
  }

  Widget textCarrera(int estado) {
    switch (estado) {
      case 0:
        return Text("Estado: En espera");
      case 1:
        return Text("Estado: Aceptado");
      case 2:
        return Text("Estado: Rechazado");
      default:
        return Text("Estado: Rechazado");
    }
  }

  void handleState() {
    setState(() {});
  }

  mostrarAlerta() {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                title: Text('Opciones'),
                content: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text('Selecciona un ciclo escolar: '),
                          Container(
                            padding: EdgeInsets.all(5.0),
                          ),
                          new DropdownButton(
                            value: _cicloActual,
                            items: _dropDownMenuItems,
                            onChanged: (value) {
                              _cicloActual = value;
                              setState(() {});
                            },
                          )
                        ],
                      ),
                      Divider(),
                      CheckboxListTile(
                        value: _sinRevisar,
                        title: Text('Mostrar solicitudes sin revisar'),
                        onChanged: (value) {
                          setState(() {
                            _sinRevisar = value;
                          });
                        },
                      ),
                      CheckboxListTile(
                        value: _aceptadas,
                        title: Text('Mostrar solicitudes aceptadas'),
                        onChanged: (value) {
                          setState(() {
                            _aceptadas = value;
                          });
                        },
                      ),
                      CheckboxListTile(
                        value: _rechazadas,
                        title: Text('Mostrar solicitudes rechazadas'),
                        onChanged: (value) {
                          setState(() {
                            _rechazadas = value;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Aceptar'),
                    onPressed: () {
                      setState(() {
                        handleState();
                        Navigator.of(context).pop();
                      });
                    },
                  )
                ],
              );
            },
          );
        });
  }

  mostrarAlerta2(String urlImg, String nombreAl, String numC, String carrera,
      CardSolicitudModel solicitud) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                title: Text('Solicitud'),
                content: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Image.network(urlImg),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancelar'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('Rechazar'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      mostrarAlerta3(solicitud);
                    },
                  ),
                  FlatButton(
                    child: Text('Aceptar'),
                    onPressed: () async {
                      await generatePDF(nombreAl, numC, carrera);
                      final resp = await solicitudesProvider.aceptarSol(
                          solicitud, urlPdf);
                      handleState();
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            },
          );
        });
  }

  mostrarAlerta3(CardSolicitudModel solicitud) {
    return showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return AlertDialog(
                title: Text('Solicitud'),
                content: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      TextField(
                        textCapitalization: TextCapitalization.sentences,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                          labelText: 'Nota de rechazo',
                        ),
                        onChanged: (text) {
                          _nota = text;
                        },
                      )
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancelar'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('Rechazar'),
                    onPressed: () async {
                      final resp = await solicitudesProvider.rechazarSol(
                          solicitud, _nota);
                      handleState();
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            },
          );
        });
  }

  void changedDropDownItem(String selectedCity) {
    setState(() {
      print(_cicloActual);
      _cicloActual = selectedCity;
      print(_cicloActual);
    });
  }

  Future generatePDF(String nombreAl, String numC, String carrera) async {
    final pw.Document doc = pw.Document();

    doc.addPage(pw.MultiPage(
        pageFormat:
            PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        header: (pw.Context context) {
          if (context.pageNumber == 1) {
            return null;
          }
          return pw.Container(
              alignment: pw.Alignment.centerRight,
              margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              decoration: const pw.BoxDecoration(
                  border: pw.BoxBorder(
                      bottom: true, width: 0.5, color: PdfColors.grey)),
              child: pw.Text('Portable Document Format',
                  style: pw.Theme.of(context)
                      .defaultTextStyle
                      .copyWith(color: PdfColors.grey)));
        },
        footer: (pw.Context context) {
          return pw.Container(
              alignment: pw.Alignment.centerRight,
              margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
              child: pw.Text(
                  'Page ${context.pageNumber} of ${context.pagesCount}',
                  style: pw.Theme.of(context)
                      .defaultTextStyle
                      .copyWith(color: PdfColors.grey)));
        },
        build: (pw.Context context) => <pw.Widget>[
              pw.Header(
                  level: 0,
                  child: pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                      children: <pw.Widget>[
                        pw.Text('Ceremonia de Graduación ITM',
                            textScaleFactor: 2),
                        pw.PdfLogo()
                      ])),
              pw.Paragraph(text: 'INSTITUTO TECNOLOGICO DE MORELIA'),
              pw.Paragraph(text: 'CEREMONIA DE GRADUACIÓN'),
              pw.Paragraph(
                  text: 'Le brindamos el honor a usted ' +
                      nombreAl +
                      ' , alumno de la carrera: ' +
                      carrera +
                      ', con matricula ' +
                      numC +
                      ' , de presentarse en la ceremonia de graduación la cuál se llevará a cabo en el auditorio del' +
                      ' Instituto Tecnológico de Morelia en el Horario asignado al alumno.'),
              pw.Paragraph(text: 'PROGRAMA:'),
              pw.Paragraph(text: '1.- Presentación de los graduados'),
              pw.Paragraph(text: '2.- Honores a la Bandera'),
              pw.Paragraph(
                  text: '3.- Palabras de nuestro director institucional'),
              pw.Paragraph(text: '4.- Entrega de certificados'),
              pw.Paragraph(text: '5.- Entrega de reconocimientos especiales'),
              pw.Paragraph(text: '6.- Discurso de graduación'),
              pw.Paragraph(
                  text: '7.- Presentación del vídeo hecho por los alumnos'),
              pw.Paragraph(
                  text: '8.- Entrega de diplomas a los alumnos destacados'),
              pw.Paragraph(text: '9.- Cierre y despedida'),
              pw.Paragraph(
                  text:
                      '"La educación es el arma más poderosa que puedes usar para cambiar el mundo"'),
            ]));

    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    print(tempPath);

    final File file = await File(tempPath + '/' + numC + '.pdf').create();
    await file.writeAsBytes(doc.save());
    await uploadPDF(file, numC);

    /* 
    final StorageUploadTask task = ref.putFile(file);
    
    if(task.isSuccessful){
      print('Archivo subido exitosamente');
    }

    */
  }

  Future uploadPDF(File file, String numC) async {
    final FirebaseApp app = await FirebaseApp.configure(
      name: 'test',
      options: FirebaseOptions(
        googleAppID: '1:1052834692832:web:906cd297ab4b4ca806da85',
        apiKey: 'AIzaSyAeL0T-FGWMzSzE81JI4Y8CSZIwUL1jfPA',
        projectID: 'gradu-app',
        databaseURL: 'https://gradu-app.firebaseio.com',
      ),
    );

    FirebaseStorage storage =
        FirebaseStorage(app: app, storageBucket: 'gs://gradu-app.appspot.com');

    final StorageReference ref =
        storage.ref().child('Pdfs').child(numC + '.pdf');

    print(ref.getBucket());
    final StorageUploadTask uploadTask = ref.putFile(
      file,
      StorageMetadata(
        contentLanguage: 'en',
        customMetadata: <String, String>{'activity': 'test'},
      ),
    );

    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
    urlPdf = dowurl.toString();

    print(uploadTask.isSuccessful);
  }
}
