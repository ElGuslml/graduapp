import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graduapp/src/providers/comprobante_provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:graduapp/src/share_prefs/preferencias_usuario.dart';

class ComprobantePage extends StatefulWidget {
  ComprobantePage({Key key}) : super(key: key);

  @override
  _ComprobantePageState createState() => _ComprobantePageState();
}

class _ComprobantePageState extends State<ComprobantePage> {
  //TODO: - Hay que hacer que cargue la imagen si ya se envió una solicitud
  //      - Si ya se envió la solicitud deshabilitar los iconos de foto e imagen
  //      - Si la solicitud es rechazada volver a habilitar los iconos

  File foto;
  final _prefs = PreferenciasUsuario();
  ComprobanteProvider comprobanteProvider = new ComprobanteProvider();
  bool respuesta = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Comprobante'),
        actions: <Widget>[
          _iconFoto(),
          _iconCam(),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 140.0,
              child: DrawerHeader(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _prefs.name +
                          ' ' +
                          _prefs.apellidoP +
                          ' ' +
                          _prefs.apellidoM,
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      _prefs.numC,
                      style: TextStyle(color: Colors.grey, fontSize: 14),
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              thickness: 1.0,
            ),
            ListTile(
              leading: Icon(Icons.chrome_reader_mode),
              title: Text('Home'),
              onTap: () {
                Navigator.pushNamed(context, 'home');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Cerrar Sesión'),
              onTap: () {
                _prefs.clearPrefs();
                Navigator.pushNamed(context, '/');
              },
            )
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              _mostrarText(),
              SizedBox(height: 20),
              _mostrarFoto(),
              SizedBox(
                height: 20,
              ),
              _mostrarBoton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _iconFoto() {
    if (_prefs.estado == 2 || _prefs.estado == 4) {
      return IconButton(
        icon: Icon(Icons.add_photo_alternate),
        onPressed: () {
          _seleccionarFoto();
        },
      );
    } else {
      return IconButton(
        icon: Icon(Icons.add_photo_alternate),
      );
    }
  }

  Widget _iconCam() {
    if (_prefs.estado == 2 || _prefs.estado == 4) {
      return IconButton(
          icon: Icon(Icons.camera_alt),
          onPressed: () {
            _tomarFoto();
          });
    } else {
      return IconButton(
        icon: Icon(Icons.camera_alt),
      );
    }
  }

  void _submit() async {
    if (foto != null) {
      final resp = await comprobanteProvider.subirImagen(foto);
      respuesta = resp;
      final _prefs = new PreferenciasUsuario();
      _prefs.estado = 0;
      setState(() {});
    }
  }

  Widget _mostrarText() {
    if (_prefs.estado == 2 || _prefs.estado == 4) {
      return Text('Sube tu comprobante de pago en la sección de abajo.');
    } else {
      return Text('Su comprobante ha sido enviado, espere una respuesta.');
    }
  }

  Widget _mostrarBoton() {
    if (respuesta == false && (_prefs.estado == 2 || _prefs.estado == 4)) {
      return SizedBox(
        width: double.infinity,
        child: FlatButton(
          child: Text('ENVIAR COMPROBANTE'),
          onPressed: _submit,
          color: Color.fromRGBO(255, 193, 7, 1),
          textColor: Colors.white,
        ),
      );
    } else {
      return SizedBox.shrink();
    }
  }

  _mostrarFoto() {
    if (_prefs.estado == 4) {
      if (foto != null) {
        return Image.file(foto, fit: BoxFit.cover, height: 300.0);
      } else {
        return Image(
          image: AssetImage('assets/images/no-image.png'),
          height: 300,
          fit: BoxFit.cover,
        );
      }
    } else {
      switch (_prefs.estado) {
        case 0:
          return Image.network(_prefs.urlImagen);
          break;
        case 1:
          return Image.network(_prefs.urlImagen);
          break;
        case 2:
          return Image.network(_prefs.urlImagen);
          break;
        default:
      }
    }
  }

  _seleccionarFoto() async {
    foto = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (foto != null) {
      //Limpieza
    }

    setState(() {});
  }

  _tomarFoto() async {
    foto = await ImagePicker.pickImage(source: ImageSource.camera);

    if (foto != null) {
      //Limpieza
    }

    setState(() {});
  }

/*
  //Metodo para elegir la imagen
  chooseImage() async {
    
    foto = await ImagePicker.pickImage(source: ImageSource.gallery);

    if( foto != null) {

    }

    setState(() {
      
    });

  }
*/
}
