import 'package:flutter/material.dart';
import 'package:graduapp/src/share_prefs/preferencias_usuario.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GraduApp'),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 140.0,
              child: DrawerHeader(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      _prefs.name +
                          ' ' +
                          _prefs.apellidoP +
                          ' ' +
                          _prefs.apellidoM,
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      _prefs.numC,
                      style: TextStyle(color: Colors.grey, fontSize: 14),
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              thickness: 1.0,
            ),
            ListTile(
              leading: Icon(Icons.chrome_reader_mode),
              title: Text('Comprobante'),
              onTap: () {
                if (_prefs.ciclo != '') {
                  Navigator.pushNamed(context, 'comprobante');
                }
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Cerrar Sesión'),
              onTap: () {
                _prefs.clearPrefs();
                Navigator.pushNamed(context, '/');
              },
            )
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              _crearBienvenida(),
              SizedBox(
                height: 10,
              ),
              _crearEstado(),
              SizedBox(
                height: 10,
              ),
              _crearBoton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _crearBienvenida() {
    return Text(
      'Hola, ' + _prefs.name + ' ' + _prefs.apellidoP + ' ' + _prefs.apellidoM,
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
      textAlign: TextAlign.center,
    );
  }

  Widget _crearEstado() {
    String texto = "";

    if (_prefs.ciclo != '') {
      switch (_prefs.estado) {
        case 0:
          texto =
              "Tu información de pago ya fue enviada pero aún no se ha revisado, regresa más tarde.";
          break;
        case 1:
          texto =
              "¡Tu información de pago fue aceptada! Para descargar tu invitación da tap en el botón de abajo.";
          break;
        case 2:
          texto =
              "Tu solicitud fue rechazada por el siguiente motivo: ${_prefs.razon}.";
          break;
        case 4:
          texto =
              "Tu información de pago aún no ha sido registrada, favor de subir tu comprobante de pago.";
          break;
        default:
          break;
      }
    } else {
      texto =
          "El proceso de alta ha terminado, favor de contactar con su coordinador en caso de algún problema.";
    }

    return Text(
      texto,
      textAlign: TextAlign.center,
    );
  }

  Widget _crearBoton() {
    switch (_prefs.estado) {
      case 0:
        return SizedBox.shrink();
      case 1:
        return _botonDescarga();
      case 2:
        return _botonEnviar();
      case 4:
        return _botonEnviar();
      default:
        return _botonEnviar();
    }
  }

  Widget _botonDescarga() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SizedBox(
        width: double.infinity,
        child: FlatButton(
            child: Text('DESCARGAR PDF'),
            color: Color.fromRGBO(255, 193, 7, 1),
            textColor: Color.fromRGBO(247, 245, 225, 1),
            onPressed: () {
              _launchURL();
            }),
      ),
    );
  }

  _launchURL() async {
    String url = _prefs.urlPdf;
    print(url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _botonEnviar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: SizedBox(
        width: double.infinity,
        child: FlatButton(
            child: Text('SUBIR COMPROBANTE'),
            color: Color.fromRGBO(255, 193, 7, 1),
            textColor: Color.fromRGBO(247, 245, 225, 1),
            onPressed: () {
              Navigator.pushNamed(context, 'comprobante');
            }),
      ),
    );
  }
}
