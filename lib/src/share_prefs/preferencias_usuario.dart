import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  get carrera {
    return _prefs.getString('carrera') ?? '';
  }

  set carrera(String value){
    _prefs.setString('carrera', value);
  }

  get ciclo {
    return _prefs.getString('ciclo') ?? '';
  }

  set ciclo(String value) {
    _prefs.setString('ciclo', value);
  }

  get numC {
    return _prefs.getString('numC') ?? '';
  }

  set numC(String value) {
    _prefs.setString('numC', value);
  }

  get name {
    return _prefs.getString('nombre') ?? '';
  }

  set name(String value) {
    _prefs.setString('nombre', value);
  }

  get apellidoP {
    return _prefs.getString('ap_p') ?? '';
  }

  set apellidoP(String value) {
    _prefs.setString('ap_p', value);
  }

  get apellidoM {
    return _prefs.getString('ap_m') ?? '';
  }

  set apellidoM(String value) {
    _prefs.setString('ap_m', value);
  }

  get estado {
    return _prefs.getInt('estado') ?? 0;
  }

  set estado(int value) {
    _prefs.setInt('estado', value);
  }

  get solicitud {
    return _prefs.getString('solicitud') ?? '';
  }

  set solicitud(String value) {
    _prefs.setString('solicitud', value);
  }

  get razon {
    return _prefs.getString('razon') ?? '';
  }

  set razon(String value) {
    _prefs.setString('razon', value);
  }

  get urlImagen {
    return _prefs.getString('urlImagen') ?? '';
  }

  set urlImagen(String value) {
    _prefs.setString('urlImagen', value);
  }

  get urlPdf {
    return _prefs.getString('urlPdf') ?? '';
  }

  set urlPdf(String value) {
    _prefs.setString('urlPdf', value);
  }

  clearPrefs() {
    _prefs.clear();
  }
}
